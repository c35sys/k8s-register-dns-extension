# Azure Kubernetes DNS extension with nsupdate keys

This extension is based on https://raw.githubusercontent.com/tesharp/acs-engine/register-dns-extension/extensions/register-dns/v1/register-dns.sh

This one allows key usage in nsupdate with a custom DNS server.

## DNS Key

First, you need a DNS key. Create one with the following command and configure your DNS accordingly:

```
dnssec-keygen -a HMAC-MD5 -b 512 -n HOST my_key.key
```

You should then have 2 files named like:
- Kmy_key.key.+157+12958.key
- Kmy_key.key.+157+12958.private


## Variables

3 variables are used :
- DOMAINNAME: your domain name
- DNS_KEY: your dns key with the content of Kmy_key.key.+157+12958.key
- DNS_PRIVATE_KEY: your dns private key with the content of Kmy_key.key.+157+12958.private

Another specific optional variable, SSH_CONFIG, is used to configure ssh with our needs.

## Parameters file

Create a parameters.json file with the following format

```
{
  "DOMAINNAME": "private.mydomain.com",  
  "DNS_KEY": "my_key.key. IN KEY 512 3 157 mRlWK1N6TiKrtMUdplJCj6+QLJiOXA6Uycs8kyD6QNG6ALnVfyiA565j JxgDTXPePioS9Dd0hcm1K3+ordevkQ=="
  "DNS_PRIVATE_KEY": "Private-key-format: v1.3\\nAlgorithm: 157 (HMAC_MD5)\\nKey:  mRlWK1N6TiKrtMUdplJCj6+QLJiOXA6Uycs8kyD6QNG6ALnVfyiA565jJxgDTXPePioS9Dd0hcm1K3+ordevkQ==\\nBits: AAA=\\nCreated: 20180315133102\\nPublish: 20180315133102\\nActivate: 20180315133102"
}
```

Adapt DNS_PRIVATE_KEY with "\\\n" to replace new lines.

## Transform parameters

In order to use the parameters, they should be in base64
```
cat parameters.json | base64 -w0
```

Keep the output to use it in the next section
```
eyAKICAiRE9NQUlOTkFNRSI6ICJwcml2YXRlLm15ZG9tYWluLmNvbSIsIAogICJETlNfS0VZIjogIm15X2tleS5rZXkuIElOIEtFWSA1MTIgMyAxNTcgbVJsV0sxTjZUaUtydE1VZHBsSkNqNitRTEppT1hBNlV5Y3M4a3lENlFORzZBTG5WZnlpQTU2NWogSnhnRFRYUGVQaW9TOURkMGhjbTFLMytvcmRldmtRPT0iCiAgIkROU19QUklWQVRFX0tFWSI6ICJQcml2YXRlLWtleS1mb3JtYXQ6IHYxLjNcXG5BbGdvcml0aG06IDE1NyAoSE1BQ19NRDUpXFxuS2V5OiAgbVJsV0sxTjZUaUtydE1VZHBsSkNqNitRTEppT1hBNlV5Y3M4a3lENlFORzZBTG5WZnlpQTU2NWpKeGdEVFhQZVBpb1M5RGQwaGNtMUszK29yZGV2a1E9PVxcbkJpdHM6IEFBQT1cXG5DcmVhdGVkOiAyMDE4MDMxNTEzMzEwMlxcblB1Ymxpc2g6IDIwMTgwMzE1MTMzMTAyXFxuQWN0aXZhdGU6IDIwMTgwMzE1MTMzMTAyIgp9Cg==
```

## Use the extension

In cluster.json file used with acs-engine, use the following:
```
***
    "extensionProfiles": [
      {
          "name": "register-dns",
          "rootURL": "https://gitlab.com/c35sys/k8s-register-dns-extension/raw/master/",
          "version": "v1",
          "script": "register-dns.sh",
          "extensionParameters": "eyAKICAiRE9NQUlOTkFNRSI6ICJwcml2YXRlLm15ZG9tYWluLmNvbSIsIAogICJETlNfS0VZIjogIm15X2tleS5rZXkuIElOIEtFWSA1MTIgMyAxNTcgbVJsV0sxTjZUaUtydE1VZHBsSkNqNitRTEppT1hBNlV5Y3M4a3lENlFORzZBTG5WZnlpQTU2NWogSnhnRFRYUGVQaW9TOURkMGhjbTFLMytvcmRldmtRPT0iCiAgIkROU19QUklWQVRFX0tFWSI6ICJQcml2YXRlLWtleS1mb3JtYXQ6IHYxLjNcXG5BbGdvcml0aG06IDE1NyAoSE1BQ19NRDUpXFxuS2V5OiAgbVJsV0sxTjZUaUtydE1VZHBsSkNqNitRTEppT1hBNlV5Y3M4a3lENlFORzZBTG5WZnlpQTU2NWpKeGdEVFhQZVBpb1M5RGQwaGNtMUszK29yZGV2a1E9PVxcbkJpdHM6IEFBQT1cXG5DcmVhdGVkOiAyMDE4MDMxNTEzMzEwMlxcblB1Ymxpc2g6IDIwMTgwMzE1MTMzMTAyXFxuQWN0aXZhdGU6IDIwMTgwMzE1MTMzMTAyIgp9Cg=="
      }
    ],
 
***
```

